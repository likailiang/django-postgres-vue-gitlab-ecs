Description: >
  This service runs the celery workers.

Parameters:

  GitSHA:
    Type: String
    Description: The Git SHA

  AppUrl:
    Type: "String"
    Description: "The URL for our app (e.g. mydomain.com)"
    AllowedPattern: "[a-z0-9._-]+"

  VPC:
    Description: The VPC that the ECS cluster is deployed to
    Type: AWS::EC2::VPC::Id

  ECSCluster:
    Description: Please provide the ECS Cluster ID that this service should run on
    Type: String

  DesiredCount:
    Description: How many instances of this task should we run across our cluster?
    Type: Number
    Default: 1

  DBEndpoint:
    Description: The DB Endpoint
    Type: String

  RedisEndpoint:
    Description: The Redis Endpoint
    Type: String

  ServiceRole:
    Description: An IAM Role that grants the service access to register/unregister with the Application Load Balancer (ALB).
    Type: String

  ECRBackendRepositoryURL:
    Description: The ECR repository for the backend container
    Type: String

  AWSAccessKeyId:
    Description: "AWS ACCESS KEY ID"
    Type: String

  AWSSecretAccessKey:
    Description: "AWS SECRET ACCESS KEY"
    Type: String

  DjangoSecretKey:
    Description: The Secret Key for backend, celery and beat containers
    Type: String
    NoEcho: true

Resources:

  CeleryService:
    Type: AWS::ECS::Service
    Properties:
      Cluster: !Ref ECSCluster
      DesiredCount: !Ref DesiredCount
      TaskDefinition: !Ref CeleryTaskDefinition

  CeleryTaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      Family: celery
      ContainerDefinitions:
        - Name: celery
          Essential: true
          Image: !Sub ${ECRBackendRepositoryURL}:${GitSHA}
          MemoryReservation: 128
          Command:
            - 'celery'
            - 'worker'
            - '-A'
            - 'backend.celery_app:app'
            - '-l'
            - 'info'
          Environment:
            - Name: GIT_SHA
              Value: !Ref GitSHA
            - Name: AWS_ACCESS_KEY_ID
              Value: !Ref AWSAccessKeyId
            - Name: AWS_SECRET_ACCESS_KEY
              Value: !Ref AWSSecretAccessKey
            - Name: SECRET_KEY
              Value: !Ref DjangoSecretKey
            - Name: APP_URL
              Value: !Ref AppUrl
            - Name: DEBUG
              Value: ''
            - Name: RDS_DB_NAME
              Value: postgres
            - Name: RDS_USERNAME
              Value: postgres
            - Name: RDS_PASSWORD
              Value: postgres
            - Name: RDS_HOSTNAME
              Value: !Ref DBEndpoint
            - Name: RDS_PORT
              Value: 5432
            - Name: CELERY_BROKER_URL
              Value: !Ref RedisEndpoint
            - Name: CELERY_RESULT_BACKEND
              Value: !Ref RedisEndpoint
            - Name: DJANGO_SETTINGS_MODULE
              Value: 'backend.settings.production'
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref CeleryCloudWatchLogsGroup
              awslogs-region: !Ref AWS::Region

  CeleryCloudWatchLogsGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      LogGroupName: !Ref AWS::StackName
      RetentionInDays: 365
